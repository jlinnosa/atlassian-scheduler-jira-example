package com.atlassian.jira.plugins.example.scheduler;

import com.atlassian.jira.plugins.example.scheduler.impl.AwesomePluginJobRunnerImpl;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.config.JobRunnerKey;

/**
 * @since v1.0
 */
public interface AwesomePluginJobRunner extends JobRunner
{
    /** Our job runner key */
    JobRunnerKey AWESOME_JOB = JobRunnerKey.of(AwesomePluginJobRunnerImpl.class.getName());

    /** Name of the parameter map entry where the ID is stored */
    String AWESOME_ID = "awesomeId";
}
