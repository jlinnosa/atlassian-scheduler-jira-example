package com.atlassian.jira.plugins.example.scheduler.impl;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import com.atlassian.jira.plugins.example.scheduler.AwesomeStuffDao;
import com.atlassian.jira.plugins.example.scheduler.AwesomeStuffSalJobs;
import com.atlassian.sal.api.scheduling.PluginScheduler;

import com.google.common.collect.ImmutableMap;

import org.slf4j.LoggerFactory;

public class AwesomeStuffSalJobsImpl implements AwesomeStuffSalJobs
{
    static final String KEY = AwesomeStuffSalJobsImpl.class.getName() + ":instance";
    public static final String JOB_NAME = AwesomeStuffSalJobsImpl.class.getName() + ":job";

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AwesomeStuffSalJobsImpl.class);

    private final AtomicBoolean scheduled = new AtomicBoolean();
    private final PluginScheduler pluginScheduler;  // provided by SAL
    private final AwesomeStuffDao awesomeStuffDao;

    public AwesomeStuffSalJobsImpl(PluginScheduler pluginScheduler, AwesomeStuffDao awesomeStuffDao)
    {
        this.pluginScheduler = pluginScheduler;
        this.awesomeStuffDao = awesomeStuffDao;
    }



    public void reschedule(int intervalInSeconds)
    {
        Map<String, Object> jobDataMap = ImmutableMap.of(KEY, (Object) AwesomeStuffSalJobsImpl.this);
        pluginScheduler.scheduleJob(
                JOB_NAME,                    // unique name of the job
                AwesomeStuffSalJob.class,    // class of the job
                jobDataMap,                  // key and class of the job to start
                new Date(),                  // the time the job is to start
                intervalInSeconds * 1000L);  // interval between repeats, in milliseconds
        scheduled.set(true);
        LOG.info(String.format("Task monitor scheduled to run every %d seconds.", intervalInSeconds));
    }

    public void unschedule()
    {
        try
        {
            if (scheduled.getAndSet(false))
            {
                pluginScheduler.unscheduleJob(JOB_NAME);
            }
        }
        catch (IllegalArgumentException iae)
        {
            LOG.warn("Looks like the job was not scheduled, after all", iae);
        }
    }

    AwesomeStuffDao getAwesomeStuffDao()
    {
        return awesomeStuffDao;
    }
}
