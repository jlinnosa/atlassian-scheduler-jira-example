
This is a simple example of how to use the `atlassian-scheduler` library from a JIRA plugin.
It creates several jobs that include the use of `Serializable` data in the parameter map, and
it makes use of Active Objects for its data storage.  Once the plugin has initialized, it
sets up some example jobs that will interact with the stored objects.  It logs profusely at
the INFO level so that you can watch what is going on, and exposes a troubleshooting page
for examining the scheduler's state at `BASE_URL/secure/admin/ShowSchedulerInfo.jspa`.

Needless to say, this plugin has no business on any production system.

I created a clone of this repository and modified it to work with the `atlassian-scheduler-compat`
library instead of using `atlassian-scheduler` directly.  The resulting example is available
as [atlassian-scheduler-compat-example](https://bitbucket.org/cfuller/atlassian-scheduler-compat-example).
Plugin developers that need to stay compatible with product versions that do not include
the `atlassian-scheduler` API should use SAL's `PluginScheduler` for jobs that are harmless
to run on all nodes in the cluster or `atlassian-scheduler-compat` for cluster-safety.



